module.exports = [{
    "type": 'evil functions',
    "pattern": /(<\?php|[;{}])[ \t]*@?(eval|preg_replace|chmod|system|fpassthru|assert|passthru|(pcntl_)?exec|shell_execute|call_user_func(_array)?)\s*\(/,
    "level": 9
}, {
    "type": 'b374k',
    "pattern": "'ev'.'al'",
    "level": 9
}, {
    "type": "evil replace",
    "pattern": /(eval(\s(.*))?\((\$|\$\$|\$\{\$\}|\'(.*)\'\)\))(.*))/,
    "level": 9
}, {
    "type": 'align',
    "pattern": /(\$\w+=[^;]*)*;\$\w+=@?\$\w+\(/,
    "level": 9
}, {
    "type": 'weevely3',
    "pattern": /\$\w=\$[a-zA-Z]\('',\$\w\);\$\w\(\);/,
    "level": 9
}, {
    "type": 'c99_launcher',
    "pattern": /;\$\w+\(\$\w+(,\s?\$\w+)+\);/,
    "level": 9
}, {
    "type": 'variable_variable',
    "pattern": /\${\$[0-9a-zA-z]+}/,
    "level": 9
}, {
    "type": 'too_many_chr',
    "pattern": /(chr\([\d]+\)\.){8}/,
    "level": 9
}, 
// {
//     "type": 'suspicious concat',
//     "pattern": /(\$[^\n\r]+\.){5}/,
//     "level": 1
// }, 
{
    "type": 'var_as_func',
    "pattern": /\$_(GET|POST|COOKIE|REQUEST)\s*\[[^\]]+\]\s*\(/,
    "level": 9
}, {
    "type": 'comment',
    "pattern": /\/\*([^*]|\*[^\/])*\*\/\s*\(/,
    "level": 9
}, {
    "type": 'md5_raw_password',
    "pattern": /md5\s*\(\s*\$_(GET|REQUEST|POST|COOKIE)[^)]+\)\s*===?\s*['"][0-9a-f]{32}['"]/,
    "level": 9
}, {
    "type": 'sha1_raw_password',
    "pattern": /sha1\s*\(\s*\$_(GET|REQUEST|POST|COOKIE)[^)]+\)\s*===?\s*['"][0-9a-f]{40}['"]/,
    "level": 9
}, {
    "type": 'basedir_bypass',
    "pattern": /curl_init\s*\(\s*["']file:\/\//,
    "level": 9
}, {
    "type": "Unfiltered input(probably XSS)",
    "pattern": /((echo|print)(\s\$_(GET|POST|REQUEST)+?\[(.*)\]))/,
    "level": 9
}, {
    "type": 'basedir_bypass2',
    "pattern": "/file:file:////",
    "level": 9
}
// , 
// {
//     "type": "Shell 1 mesaque",
//     "pattern": /(((shell|cmd)_exec|\$\_(.*))\(.*\)\;\s|getcwd\S(.*)|(\[\'\Schmod\'\]|\[\'.*cmd\'\])|fpassthru\(.*\))/,
//     "level":9
// }
,{
    "type": 'disable_magic_quotes',
    "pattern": /set_magic_quotes_runtime\s*\(\s*0/,
    "level": 9
}, {
    "type": 'code execution try',
    "pattern": /(eval|assert|passthru|exec|include|system|pcntl_exec|shell_execute|base64_decode|`|array_map|call_user_func(_array)?)\s*\(\s*(base64_decode|php:\/\/input|str_rot13|gz(inflate|uncompress)|getenv|pack|\\?\$_(GET|REQUEST|POST|COOKIE))/,
    "level": 9
}, {
    "type": 'deep code execution try',
    "pattern": /(array_filter|array_reduce|array_walk(_recursive)?|array_walk|assert_options|uasort|uksort|usort|preg_replace_callback|iterator_apply)\s*\(\s*[^,]+,\s*(base64_decode|php:\/\/input|str_rot13|gz(inflate|uncompress)|getenv|pack|\\?\$_(GET|REQUEST|POST|COOKIE))/,
    "level": 9
}, {
    "type": 'reverse engienering code execution try',
    "pattern": /(array_(diff|intersect)_u(key|assoc)|array_udiff)\s*\(\s*([^,]+\s*,?)+\s*(base64_decode|php:\/\/input|str_rot13|gz(inflate|uncompress)|getenv|pack|\\?\$_(GET|REQUEST|POST|COOKIE))\s*\[[^]]+\]\s*\)+\s*;/,
    "level": 9
}, {
    "type": 'htaccess',
    "pattern": "SetHandler application/x-httpd-php",
    "level": 9
}, {
    "type": 'iis_com',
    "pattern": /IIS:\/\/localhost\/w3svc/,
    "level": 9
}, {
    "type": 'include',
    "pattern": /include\s*\(\s*[^\.]+\.(png|jpg|gif|bmp)/,
    "level": 9
}, {
    "type": 'ini_get',
    "pattern": /ini_(get|set|restore)\s*\(\s*['"](safe_mode|open_basedir|disable_function|safe_mode_exec_dir|safe_mode_include_dir|register_globals|allow_url_include)/,
    "level": 9
}, {
    "type": 'suspicious preg_replace',
    "pattern": /(preg_replace(_callback)?|mb_ereg_replace|preg_filter)\s*\(.+(\/|\\x2f)(e|\\x65)['"]/,
    "level": 9
}, {
    "type": 'safemode_bypass',
    "pattern": /\x00\/\.\.\/|LD_PRELOAD/,
    "level": 9
}, {
    "type": 'shellshock',
    pattern: /\(\)\s*{\s*:\s*;\s*}\s*;/,
    "level": 9
}, {
    "type": 'udp_dos',
    pattern: /fsockopen\s*\(\s*['"]udp:\/\//,
    "level": 9
}]
