var Stux = {
	init: function(){
		this.initListeners();
		this.initWS();
	},
	showMessage: function(icon, text, type){
		$('.message').fadeIn();
		$('.message').find('p').text(text);
		$('.message').find('i').addClass(icon);
		$('.message').addClass(type).fadeIn();

		setTimeout(function(){
			$('.message').hide();
			$('.message').find('i').removeClass(icon);
			$('.message').removeClass(type);
		}, 5000);
	},
	initListeners: function(){

		var _that = this;

		function scanSite(){
			var $el = $(this);
			
			var hash = $el.data('hash');
			
			// beguinScan($(this).data('hash'));
			$.post('/site/scan', {hash: hash }, function(){
				beguinScan($el.data('hash'));
				_that.showMessage( 'ion-ios-checkmark', 'Auditoria iniciada', '' );
			}).fail(function(e) {

				var e = JSON.parse(e.responseText);
				console.log(e);
				if( e.error ){
					_that.showMessage( 'ion-alert-circled', e.msg, 'error' );
					return;
				}

				
			});
		}


		function endScan(){
			$('.btn-view').addClass('s-hide');
			$('.btn-scan').removeClass('s-hide');
			$('.waiting').addClass('done');


			// setTimeout(function(){
			// 	$('.watiting').addClass('s-hide');

			// }, 2000);
		}

		function beguinScan(hash){
			$('#progress-'+ hash +' .btn-view').removeClass('s-hide');
			$('#progress-'+ hash +' .btn-scan').addClass('s-hide');
			$('#progress-'+ hash +' .waiting').removeClass('s-hide');
			$('#progress-'+ hash +' span.state-caption').addClass('s-hide');
		}
		

		function saveSite(){
			var data = {
				url: $('#site_url').val(),
				active: 1,
				date: +new Date,
				hash: $('#site_hash').val(),
				user: $('#user_id').val()
			}

			$.post('/site/add', data, function(r){
				_that.showMessage( 'ion-ios-checkmark', 'Site adicionado.', '' );
			}).fail(function(){
				_that.showMessage( 'ion-alert-circled', 'Algo deu errado.', 'error' );
			})
		}

		function verifySite(){
			var site_url = $('#site_url').val();
			var site_hash = $('#site_hash').val();


			if( !site_url || site_url === "" ) $('#site_url').addClass('err');

			$.post('/site/verify', { 
				url 	: site_url,
				timex 	: (+new Date()), 
				key 	: site_hash 
			}, function(r){
				_that.showMessage( 'ion-ios-checkmark', r.caption, '' )
				$('.save').slideDown();
			}).fail(function(err){
				err = JSON.parse(err.responseText);
				_that.showMessage( 'ion-alert-circled', err.caption, 'error' );

			});
		}

		$('.tooltip-aria').tooltipster({
			theme: 'tooltipster-borderless',   
			delay: 10,
			trigger: 'hover'
		});
		$('.save-settings').on('click', function(){
			$.post(
				'/settings', { 
					reg: JSON.stringify( $("#regex-form").serializeJSON().reg ) }, 
					function(e){
				if( e ){
					_that.showMessage( 'ion-ios-checkmark', e.msg, 'success' );
					setTimeout(function(){
						window.location.reload();
					}, 2000);
				}

			});
		});

		$('body').on('click', '.edit-regex', function(){
			var $top = $(this).closest('.regex-view');
			console.log('ONEDIT ->',$top);	

			$('.regex-view').removeClass('editing')
			$top.addClass('editing');
			updatePre();
		})	
		$('.edit-site-call').on('click', function(){
			var $index = $(this).data('index');
			// $('#myModal').find('.modal-body').text('ITEM ->'+ JSON.stringify(mySites[$index]));
			$('#site-url').val(mySites[$index].url)
			$('#stux-file').val('stux-'+mySites[$index].hash+'.php');

            $('strong#site-title').text('#'+mySites[$index].id)

			if( mySites[$index].active == 1 ){
				$('[value="1"]').prop("checked", true);
			}else{
				$('[value="0"]').prop("checked", true);

			}
			$('#edit-site').modal('show');
		});

		$('.save-site-changes').on('click', function(){
			var $siteUrl 	= $('#site-url').val();
			var $state 		= $('[name="site-state"]:checked').val();
			var $id 		= $('strong#site-title').text().split('#')[1];

			var postData = {
				url: $siteUrl,
				state: $state,
				id: $id
			}
			$.post('/site-meta', postData, function(res){
				_that.showMessage( 'ion-ios-checkmark', 'Alterações salvas', 'success' );
				setTimeout(function(){
					window.location.reload();
				}, 2000);
			});	
		});

		$('.delete-site-call').on('click', function(){
			$('#confirm-delete').modal('show');
			$index = $(this).data('index');
		});

		$('#delete-site-value').on('keyup', function(){
			if( $(this).val() === mySites[$index].url ){
				$('.confirm-delete').removeAttr('disabled');
			}else{
				$('.confirm-delete').attr('disabled', true);

			}
		});

		$('body').on('click', function(e){
			if( $('.report-yeld').is(':visible') ){
				if( e.target.classList[0] == 'show-detail'){
					return;
				}
				
				if( e.target.classList[0] !== 'report-yeld'){
					$('.detail').fadeOut();
				}
			}
		})
		$('.confirm-delete').on('click', function(){
			$.post('/site-delete', {id: mySites[$index].id}, function(res){
				_that.showMessage( 'ion-ios-checkmark', 'Dados apagados!', 'success' );
				setTimeout(function(){
					window.location.reload();
				}, 2000);			
			});
		})
		$('.show-detail').on('click', function(){
			$(this).parent().parent().find('.detail').fadeIn();
		});
		$('body').on('click', '.regex-save', function(){
			var $top;
			$top = $(this).closest('.regex-view');

			var desc = $top.find('[name="reg[][type]"]').val();		
			var value = $top.find('[name="reg[][pattern]"]').val();	

			console.log('TOP', $top)
			$top.find('.val').text( value );
			$top.find('.desc').text( desc );
			$top.removeClass('editing');
			updatePre();
		});


		function updatePre(){
			console.log('CALLED')
			$('#myPre').text('');
			$('#myPre').text(JSON.stringify($("#regex-form").serializeJSON().reg))
			$('.regex-cancel').on('click', function(){
				$('.regex-view').removeClass('editing');
			});	
		
		}
		$('#site_url').on('keyup', function(){
			$(this).removeClass('err');
		});
		$('.delete-regex').on('click', function(){
			var $top = $(this).parent().parent().parent();	
			$top.remove();
			console.log('.....')
		});

		$('.add-regex').on('click', function(){
			var $markup = ['<div class="regex-view">',
			'	<div class="col-sm-12">',
			'		<div class="col-sm-12">   ',
			'			<div class="regex-item">',
			'				<label>Descrição</label>',
			'				<input type="text" value="" name="reg[][type]" class="form-control">',
			'				<div class="value-item desc">Descreva, faça observações sobre o padrão</div>',
			'				<label>Regex</label>',
			'				<input type="text" value="" name="reg[][pattern]" class="form-control">',
			'				<div class="value-item val">/Padrão/</div>',
			'			</div>',
			'		</div>',
			'	</div>',
			'		<div class="col-sm-12">',
			'		<a href="javascript:void(0)" class="btn edit-regex">Editar</a>',
			'		<a href="javascript:void(0)" class="btn delete-regex">Excluir</a>',
			'		<a href="javascript:void(0)" class="btn regex-save">Ok</a>',
			'		<a href="javascript:void(0)" class="btn regex-cancel">Cancel</a>',
			'		</div>',
			'</div>'].join("");	
			$('.controls').before($markup);
		updatePre();
			

		});
		$('.btn-save').on('click', saveSite)
		$('.btn-verify').on('click', verifySite);
		$('.btn-scan').on('click', scanSite);
		$('table.paginated').each(function() {
		    var currentPage = 0;
		    var numPerPage = 10;
		    var $table = $(this);
		    $table.bind('repaginate', function() {
		        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
		    });
		    $table.trigger('repaginate');
		    var numRows = $table.find('tbody tr').length;
		    var numPages = Math.ceil(numRows / numPerPage);
		    var $pager = $('<div class="pager"></div>');
		    for (var page = 0; page < numPages; page++) {
		        $('<span class="page-number"></span>').text(page + 1).bind('click', {
		            newPage: page
		        }, function(event) {
		            currentPage = event.data['newPage'];
		            $table.trigger('repaginate');
		            $(this).addClass('active').siblings().removeClass('active');
		        }).appendTo($pager).addClass('clickable');
		    }
		    $pager.insertAfter($table).find('span.page-number:first').addClass('active');
		});
	},
	initWS: function(){
		var socket = io(STUXNET.WS.url, {path: '/stux'});
		socket.on('state', function(e){
			if( $('#output-'+e.key).length > 0 ){					
				$('#output-'+e.key).find('#output').append(
					'<br />['+e.percent+'%] file '+e.currentFile+' of '+e.totalFiles+' files <strong>'+e.fileName+'</strong>' 
				);
				$("#output").scrollTop($("#output")[0].scrollHeight);
			}

			$('[data-id="'+e.scanId+'"]').find('.percent-analisys').text(e.percent+' %')
			$('[data-id="'+e.scanId+'"]').find('.file-analisys').text(e.currentFile )
			if( $('#progress-'+e.key).length > 0 ){
				$('#progress-'+e.key).find('.progress-fill').css('width',e.percent+'%');
			}
		})


		socket.on('endScan', function(e){
			$('.btn-view').addClass('s-hide');
			$('.btn-scan').removeClass('s-hide');
			$('.waiting').addClass('done');


			if( e.result.length <= 5 ){
				$('#progress-'+ e.key)
				.find('.waiting')
				.find('.progress-fill')
				.addClass('less');
			}
			
			if( e.result.length >= 5 ){
				$('#progress-'+ e.key)
				.find('.waiting')
				.find('.progress-fill').addClass('midle');
			}

			if( e.result.length > 6 ){
				$('#progress-'+ e.key).find('.waiting').find('.progress-fill').addClass('midle');
			}					
			setTimeout(function(){
				//- $('.waiting').addClass('s-hide');
			}, 2000);
		})

	}
}


window.onload = Stux.init();