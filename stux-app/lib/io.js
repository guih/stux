var socket_io = require('socket.io');
var io        = socket_io(1999, {path :'/stux'});
var socketApi = {};
var scan      = require('./scans');

socketApi.io = io;

io.on('connection', function(socket){
    console.log('> A user connected to socket...');
});

socketApi.sendNotification = function(event, data) {
	if( event == 'state' ){
	 	if( parseInt(data.currentFile) == data.totalFiles - 1){
	 		data.percent = '100.0';
	 	}
	 	
 		scan.updateMeta( data.scanId, data, function(){
 			io.sockets.emit(event, data);
 		} )	;
	}

	if( event == 'state-end' ){
		setTimeout(function(){
	 		scan.update( data.scanId, 'result', JSON.stringify(data), function(){
	 			io.sockets.emit('finished', { hash: data.scanId  });
	 		} )	;		
		},1000);
	}



}

socketApi.analisysWatch = function(data) {
    io.sockets.emit('analisys-watch', data);
}

module.exports = socketApi;