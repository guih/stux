var conf = require('../config');

var env = process.env.NODE_ENV || "development";
var auth = {};

if( env == 'development' ){ 
	auth = {
		client: 'mysql',
		connection: {
			host : 'localhost',
			user : 'root',
			password : '',
			database : 'stuxnet'
		}
	};
}


if( env == 'production' ){
	auth = { 
		client: 'mysql',
		connection: {
			host 		: conf.DB.HOST,
			user 		: conf.DB.USER,
			password 	: conf.DB.PASS,
			database 	: conf.DB.DATABASE
		}
	};
}


var knex = require('knex')(auth);

module.exports = knex;