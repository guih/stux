var crypto = require('crypto');
var moment = require('moment');
var fs     = require('fs');

function _crypt(data) {
    return crypto.createHash('md5').update(data).digest('hex');
}
function _deep_crypt(data){
	return crypto.createHash('sha256').update(data).digest('hex');
}

function _format_date(ts, fn){
	return moment(ts).format("DD/MM/YYYY HH:mm:ss");
}

function encrypt(text){
  var cipher = crypto.createCipher('aes-256-cbc','d6F3Efeq')
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-cbc','d6F3Efeq')
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}


function _json_encode(json, fn){
	var parsed;

	try{
		parsed = JSON.parse( json );
		fn(parsed);
	}catch(e){
		console.log('EXCEPTIOn =>', e);
		return fn({});
	}	
}

function _generate_stux_file(fileName, res, type, fn){
	var stuxClient = './php/stux.php';

	fs.readFile(stuxClient, function(err, data) {
	    if(err) throw err;
	    data = data.toString();
	    data = data
	    .replace(
	    	/(private\s\$key\s=\s\'(.*)\'\;)/g, 
	    	   	'private $key = \''+fileName+'\''+';'
	    );

	    if( type == 'checksum' || !res ){
	    	fn(crypto
		    	.createHash('md5')
		    	.update(data)
		    	.digest('hex'));
	    return;
	    }
	    
	    fs.writeFile(stuxClient, data, function(err) {
	        err || console.log('Data replaced! \n', data);
	        
			res.download(stuxClient, 'stux-'+fileName+'.php', function(err){
			  if (err)  return;
			});
	    });
	});
}
module.exports = {
	hash: _crypt,
	shaHash: _deep_crypt,
	formatDate: _format_date,
	encrypt: encrypt,
	decrypt: decrypt,
	parseJSON: _json_encode,
	clientGenerator: _generate_stux_file
}