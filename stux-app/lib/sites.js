var db      = require('./db');
var request = require('request');
var util    = require('util');
var utils   = require('./utils');
var stux    = require('./stux');
var io      = require('./io');
var scans    = require('./scans');


function _site_connection_exists( site, hash, fn ){
  _httpRequest({ 
    domain  : site, 
    key     : hash, 
    action  : 'ping'
  }, function(err, body){

    if( err ) return fn( err, null );
    utils.parseJSON( body, function(parse){

      fn( null, parse );
    } );

  })
}


function _httpRequest(args, fn){

  if( !args.domain ){
    fn('No domain suplied', null);
    return;
  } 

  if( !args.key ){
    fn('No key suplied', null);
    return;
  } 

  if( args.domain.charAt(args.domain.length-1) != '/' )
    args.domain = args.domain+'/'

  // console.log(util.format( '%sstux-%s.php', args.domain, args.key ))
  request.post({
    url: util.format( '%sstux-%s.php', args.domain, args.key ), 
    form: {
      key: args.key,
      action: args.action,
      file: args.file || null
    }
  }, function(err,res,body){

    if( !res ){
      return fn(null, {error: 'undefined'});
    }
    if (!err && res.statusCode == 200){
      return fn(null, body);
    }
    if( res.statusCode == 404 ){
      return fn({ error: "404 - Not found "}, null);
    }

    if( res.statusCode == 401 && body.indexOf( 'KEY' ) != -1 )
      return fn({ error: "401 - Unauthorized "}, null);


    fn({ error: "Cant acess interface."}, null);
  }); 
}

function _getSites(id, fn){
  db('sites')
  .select('*')
  .where('user', id)
  .then( function (result) {
    if( result.length > 0 ){}
    fn(null, {result: result });
  }) 
}
function _by_hash( hash, fn ){
  db('sites')
  .select('*')
  .where('hash', hash)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No site found."}, null );
      return;
    }
    // console.log(result);
    fn( null, result[0] );    
  });
}


function _scan_by_user_id( id, fn ){
  db('scan')
  .select('*')
  .where('user', id)
  .then( function (result) {
    if( result.length === 0 ){

      fn( {error: "No scan found."}, null );
      return;
    }

    fn( null, result );    
  });
}
function _scan_by_id( scanId, fn ){
  db('scan')
  .select('*')
  .where('id', scanId)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No scan found."}, null );
      return;
    }

    fn( null, result[0] );    
  });
}

function _drop_website(id, fn){
  id = parseInt(id);
    db("sites")
    .where("id", id)
    .delete().then(function (count) {
      db("scan")
      .where("site", id)
      .delete().then(function (count) {
        fn({msg: 'All data loss!'});
      })
    })

}

function _update_site_state(id, state, fn){
  _scan_by_id( id, function(err, scan){ 
    db("sites")
    .where("id", id)
    .update({state: state}).then(function (count) {
      fn();
    })
  } )
}
function _update_site_front_end(id, state, url, fn){
    db("sites")
    .where("id", id)
    .update({active: state, url: url}).then(function (count) {
      fn();
    })
}


function _update_scan(id, meta, fn){
  _scan_by_id( id, function(err, scan){ 
    scan.meta += meta; 

    var  myMeta = JSON.parse(scan.meta); 
    db("scan")
    .where("id", id)
    .update({
      meta: meta, 
      percent: myMeta.percent, 
      file: myMeta.currentFile
    }).then(function (count) {
      fn()
    })
  } )
}
function _create_scan(user, site, fn){

  if( !user ) fn({error: "Not loggedin."});

  db('scan')
  .insert({
    user: user.id,
    meta: '[]',
    date: +new Date,
    site: site.id,
    result: '[]',
    parent_meta: JSON.stringify( site )
  })
  .then( function (result) {

    if( !result || isNaN( result ) ){
      fn({error: "An error ocurred while we tryin to add this website."});
      return;
    }

    fn(null, {result: result, args: arguments});
  }) 
}
function _update_state(){

}
function _stux_ready(user, site, fn){

  if( site.active == 0  ){
    fn( { error : true, state: 'stoped', msg: 'Inactive websites can\'t be scaned.' }, null )
    return;
  }
  _create_scan(user, site, function(error, scanResult){
    // console.log(site.hash);
    _by_hash( site.hash, function(error, site){
        _update_site_state( site.id, 'working', function(){
          stux( site.url, site.hash, scanResult.result[0], function(error, res){
            io.sendNotification('endScan', res);
              _update_site_state( site.id, '', function(){
               
              });
          } );
          fn(null, {error: false, state: 'scaning', msg: 'Scan initialized...'});
        } );
    } );
  });
}
function _by_id( id, fn ){
  db('sites')
  .select('*')
  .where('id', id)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No site found."}, null );
      return;
    }
    // console.log(result);
    fn( null, result[0] );    
  });
}
function _saveSite(args, fn){
  db('sites')
  .insert(args)
  .then( function (result) {

    if( !result || isNaN( result ) ){
      fn({error: "An error ocurred while we tryin to add this website."});
      return;
    }

    fn(null, {result: result, args: args});
  }) 
}
function _editSite(args, fn){

}
function _deleteSite(args, fn){

}

function _isActive(site, state, fn){

    db("sites")
    .where("id", site.id)
    .update({active: parseInt(state)}).then(function (count) {
      console.log('count', count);
      fn();
    })
}

function _fixBroken(fn){

    db("sites")
    .where("state", 'working')
    .update({state: ''}).then(function (count) {
      _fixBrokenScan(function(){
        fn();
      })
    })
}

function _fixBrokenScan(fn){

    db("scan")
    .whereRaw("percent < ?", ['100'])
    .delete().then(function (count) {
      fn();
    })
}

function _getSiteMeta(sites, fnExport){
  function getSiteScans(sites, fn){
    var result = [];
    var count = 0;
    var item = null;


    require('async').whilst(
        function () { return (count  + 1) <= sites.length; },
        function (callback) {
            item = sites[count];
            count++;

            scans.getBySite( item.id, function(err, rows){
              item.scans = rows;
              result.push(item);              
              callback(null, result)
            } );
        },
        function (err) {
           fn(null, result)
        }
    );        
  }
  function processSiteState(sites, fn){


    var result = [];
    var count = 0;
    var item = null;


    require('async').whilst(
        function () { return count <= sites.length-1; },
        function (callback) {

            item = sites[count];
            console.log('Checking %s...', sites[count].url);
            _site_connection_exists( sites[count].url, sites[count].hash, function(err, state){
                sites[count].date = utils.formatDate( parseInt( sites[count].date ) )
      
                sites[count].connected = false;

                console.log('[ i ] Site connection status is %s...', state ?  state.state : err.error);

                // console.log(sites[count].url, state, err, 'CURRENT...')
                if( state ){
                  sites[count].connected = null
                  sites[count].connected = true;
                }
              

              result.push(item);
              count++;
              callback(null, null);
            })  
        },
        function (err) {
           fn(null, sites)
        }
    );      
  }

  require('async').waterfall([
      function parseDate(callback) {
        sites.map(function(site){
          return site.date = utils.formatDate( parseInt( site.date ) );
        });

          callback(null, sites);
        // callback(null, parseTime);
      },
      function checkConnection(sitesTParsed, cb) {
          var nSite = [];
          var sitesReturned;

          processSiteState(sitesTParsed, function(err, results){
            cb(null, results);
          });  
      },
      function getScans(sitesTParsed, cb){

          getSiteScans(sitesTParsed, function(err, results){
            cb(null, results);
          });          
      },
  ], function (err, result) {
      fnExport( null, result );
  });
}

module.exports = {
  check   : _site_connection_exists,
  get     : _getSites,
  byId    : _by_id,
  byHash  : _by_hash,
  fix: _fixBroken,
  add     : _saveSite,
  edit    : _editSite,
  delete  : _drop_website,
  scan    : _stux_ready,
  scanById    :_scan_by_id,
  userScans   : _scan_by_user_id,
  updateScan  : _update_scan,
  toggleActive: _isActive,
  getSitesMeta: _getSiteMeta, 
  updateOutsite: _update_site_front_end,
};