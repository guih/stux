var config = require('../config');
var env = process.env.NODE_ENV || "development";
var cbURI = null;


if( env == 'production' ){ 
	cbURI = "https://security.apiki.com/auth/google/callback";
}

if( env == 'development' ){
	cbURI = config.baseURI + ":3000/auth/google/callback";
}


console.log(cbURI)

module.exports = {
	CLIENT_ID		: "344087705778-occp2m7je0hilpbu28nljm0d9dut4o1e.apps.googleusercontent.com",
	CLIENT_SECRET	: "035wFXZZqIxLLGFsruUh1C4M",
	CALLBACK_URL	: cbURI,
	CLIENT_SCOPE	: [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
    ]
};