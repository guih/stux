var io     = require('./io');
var db     = require('./db');
var site   = require('./sites');
var moment = require('moment');



function _update_scan_meta(id, meta, fn){

  _scan_by_id( id, function(err, scan){ 
  	require('./utils').parseJSON(scan.meta, function(json){

	  	var myScan = [];
	  	myScan.push(meta);

	  	myScan = myScan.concat(json);	  	
	    newMeta = JSON.stringify(myScan);  

    // io.analisysWatch( {
    //   meta     : newMeta, 
    //   percent  : parseFloat( t[0].percent), 
    //   file     : parseInt(t[0].currentFile) 
    // } );
	    setTimeout(function(){    	
		    db("scan")
		    .where("id", id)
		    .update({
          meta    : newMeta, 
          percent : parseFloat( myScan[0].percent ), 
          file    : parseInt( myScan[0].currentFile ) 
        }).then(function (count) {
		      fn()
		    })
	    }, 1000); 
  	});

  } );
}
function _update_scan(id, key, meta, fn){
    db("scan")
    .where("id", id)
    .update(key, meta).then(function (count) {
      fn()
    });
}

function _scan_by_id( scanId, fn ){
  db('scan')
  .select('*')
  .where('id', scanId)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No scan found."}, null );
      return;
    }

    fn( null, result[0] );    
  });
}

function _scan_by_site( siteId, fn ){
  db('scan')
  .select('*')
  .where('site', siteId)
  .then( function (result) {


    if( result.length === 0 ){
      fn( {error: "No scan found."}, null );
      return;
    }
    moment.locale('pt-br');
    result.map(function(scan){
      scan.parsedTime = moment(parseInt(scan.date)).fromNow();
      scan.pasedX = moment(parseInt(scan.date)).format("YYYY-MM-DD HH:mm:ssZ");
      return scan;
    });

    // console.log(result);

    fn( null, result );    
  });
}

exports.updateMeta = _update_scan_meta;

exports.update = _update_scan;
exports.getBySite = _scan_by_site;