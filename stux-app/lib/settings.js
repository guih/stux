var io     = require('./io');
var db     = require('./db');
var site   = require('./sites');
var moment = require('moment');

exports.updateSetting = function(setting, value, fn){
	db("settings")		
	    .update({
			value    : value 	
    	}).
	    where('name', setting)
	    .then(function (count) {
	    
	    fn();
	});
}
exports.getSettings = function(fn){
	db("settings")
	   .select('*')
	   .then(function (settings) {
	   		fn(settings);
	});
}