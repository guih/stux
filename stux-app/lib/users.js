var db = require('./db');

function _add_user( args, fn ){
  db('users')
  .insert(args)
  .then( function (result) {
    fn({result: result, args: args});
  }) 
}

function _user_exists( email, fn ){
  db('users')
  .select('*')
  .where('email', email)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No user found."}, null );
      return;
    }

    fn( null, result[0] );    
  });
}
function _by_id( id, fn ){
  db('users')
  .select('*')
  .where('id', id)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No user found."}, null );
      return;
    }

    fn( null, result[0] );    
  });
}

function _by_key( key, fn ){
  db('users')
  .select('*')
  .where('key', key)
  .then( function (result) {
    if( result.length === 0 ){
      fn( {error: "No user found."}, null );
      return;
    }

    fn( null, result[0] );    
  });
}

function _by_key_bool(key, fn){
  _by_key( key, function ( err, record ){
    if( err ) return;

    return true;
  } );
}
function _check_logged_state(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.redirect('/login');
  }
}


module.exports = {
  add: _add_user,
  check: _user_exists,
  byId: _by_id,
  byKey: _by_key,
  keyExists: _by_key_bool,
  isLoggedIn: _check_logged_state
};
