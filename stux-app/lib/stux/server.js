/**
 * Module dependencies.
 */
var express = require('express');


module.exports = function(data){	
	var app = express();
	app.use(express.static(__dirname));

	app.set('views', __dirname);
	app.set('view engine', 'jade');

	app.get('/', function(req, res){
	  res.render('index', { m: data });
	});
	process.stdout.clearLine();
	process.stdout.cursorTo(0);

	app.listen(1333);

	console.log('Express app started on port %d', 1333);
};