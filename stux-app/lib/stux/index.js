
module.exports = function(domain, key, scanId, fn){
	var io = require('../io');
	var stux = require('./lib');
	var server = require('./server');
	require('../settings').getSettings(function(settings){
		var regex = settings.filter(function(setting){
			return setting.name == 'regex';
		});
		var options = {
			domain	: domain,
			key		: key,
			scanId	: scanId,
			verbose	: true,
			timeout : 100,
			patterns: JSON.parse(regex[0].value)
		};

		stux.scan(options, function(err, result){

			if( err ) return console.log(err);
			
			fn(null, {
				domain	: domain,
				key		: key,
				result: result,
				scanId	: scanId,
				verbose	: false,
				timeout : 100 
			});
		});	
	});


}
