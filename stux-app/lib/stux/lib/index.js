/*!
 * Stux
 * Copyright(c) 2016 Guilherme Henrique <guihknx[at]gmail.com>
 * MIT Licensed
 */

'use strict'

var fs       = require('fs');
var util     = require('util');
var async    = require('async');
var colors   = require('colors');
var request  = require('request');
var patterns = require('./patterns');
var io       = require('../../io');
var utils    = require('../../utils');
var verbose  = null;
var timeout  = null;


var STUX = {
	VERSION: '0.0.1',
};

function scanFiles(args, cb){
	var domain = args.domain,
	key = args.key,
	scan = args.scanId,
	result = [];


	patterns = args.patterns || paterns;


	// verbose flag must be global
	verbose = args.verbose || null;
	timeout = args.timeout || 100;



	async.waterfall([
	    countFiles,
	    handleFiles,
	], function (err, result) {
	    cb(err, result);
	});

	function countFiles(fn) {
		getFileCount(domain, key, function(err, fileCount){
			if( err ) return fn(err, null);
			fn(null, fileCount);
		});
	}

	function handleFiles(count, fn) {
		var fileIndex = 1,
		currentFile, 
		percent, 
		fileParts,
		fileName, 
		output;

		async.whilst(
		    function () { return fileIndex <= count-1; },
		    function (fn) {

		    	function readFile(err, chunk){
					if( err ) return fn(err, null);

	    			currentFile = ( new String( fileIndex ) ).bold;
	    			percent 	= getPercent(fileIndex, count);
	    			fileParts 	= chunk.file.split('/');
	    			fileName 	= fileParts[ fileParts.length - 1 ];

	    			output = [
		    			'file',
		    			currentFile,
		    			'of',
		    			count.bold,
		    			percent+'%',
		    			'filesize:',
		    			chunk.size,
		    			fileName.magenta,
		    			'\x1B[0G'
		    		].join(" ");

					io.sendNotification('state', {
						currentFile 	: new String( fileIndex ),
						totalFiles 		: count,
						percent 		: percent,
						fileName 		: chunk.file,
						filesize 		: chunk.size,
						key 			: key,
						scanId 			: scan
					});


					setTimeout( function () {
						var threat = [];
						var parsedArray = null;
						readLines( chunk.contents, function(end){
							if( end.length !== 0 ){
								for( var i = 0; i< end.length; i++ ){
									threat.push(end[i][0]);

									if( i == end.length - 1 ){
										result[fileIndex] = { 
											threat: threat,
											file: chunk.file
										};		
									}
								}
							}
							
							result = result.filter(function(isEmpty){ 
								return isEmpty !== null 
							}); 
							setTimeout(function(){
								fn(null, result);					
							}, 1000)
						} );	
						fileIndex++;	    		
					}, 100);
		    	}


	    		fileGetContents(domain, key, fileIndex, readFile);
		    },
		    function (err, n) {
				fn(null, result);
				result.scanId = scan;
				io.sendNotification('state-end', result);
		    }
		);
	}
}


function readLines(file, fn){

	var file = new Buffer(file, 'base64').toString("ascii");

	var lines = file.split(/\r|\n/);
	var threat = [];

	lines.forEach(function(line, index){

		searchPattern(line, index, function(found){
			
			if( found.length > 0 ) threat.push(found);
			if( lines.length - 1 == index )	fn(threat);		
		});

	});
}

function jsonResponseHandler(json){

	var data;
	try{
		data = JSON.parse(json);
	}catch(e){
		data = "Internal error";
	}

	return data;
}

function consoleWrite(str){
	var spawn = process.stdout || {};
	var now   = new Date();

	var nowReadable = [	
		now.toLocaleDateString(),
		now.toLocaleTimeString()
	].join(' - ');

	spawn.clearLine();
	spawn.clearLine();
	spawn.cursorTo(0);
	spawn.write(['  [', '] '].join( nowReadable ).green.bold);
	spawn.write(str);
}

function httpRequest(args, fn){

	if( !args.domain ){
		fn('No domain suplied', null);
		return;
	} 

	if( !args.key ){
		fn('No key suplied', null);
		return;
	} 

	if( args.domain.charAt(args.domain.length-1) != '/' )
		args.domain = args.domain+'/'

	
	if( verbose )
		console.log('acess => '+util.format( '%sstux-%s.php', args.domain, args.key )+' with params'+ JSON.stringify(args));


	// console.log(util.format( '%sstux-%s.php', args.domain, args.key ));
	request.post({
		url 	: util.format( '%sstux-%s.php', args.domain, args.key ), 
		form 	: {
			key 	: args.key,
			action 	: args.action,
			file 	: args.file || null
		}
	}, function(err,res,body){

		if( !res ){
			fn({ error: "Cant acess interface.", file: args.file}, null);
			return;
		}

		if (!err && res.statusCode == 200)
			return fn(null, body);

		if( res.statusCode == 401 && body.indexOf( 'KEY' ) != -1 )
			return fn({ error: "401 - Unauthorized"+body}, null);


	});	
}

function getPercent(current,  of){
	return ( ( current / of ) * 100 ).toFixed(1) || 0;
}

function searchPattern(line, lineNumber, fn){
	var found = [];

	console.log('>>>>>>>>>>>>>>>>>>>>>>>>', patterns);
	patterns.forEach(search);

	function search(pattern, interator){
		var filtered = line.match( new RegExp(pattern.pattern , 'g'));

		var reg = pattern.pattern+"";

		if( filtered ) {
			found.push({ 
				matches : filtered, 
				at 		: lineNumber+1, 
				type 	: pattern.type, 
				regex 	: reg, 
				level 	: pattern.level || 9,
				chunk 	: utils.encrypt( line )
			});
		}

		if( patterns.length - 1 ==  interator) fn( found );		

	}
};

function getFileCount(domain, key, fn){
	httpRequest({ 
		domain 	: domain, 
		key 	: key, 
		action 	: 'getFileCount'
	}, function(err, body){

		if( err ) return fn( err, null );

		fn(null, body);
	})
}


function fileGetContents(domain, key, index, fn){

	httpRequest({ 
		domain 	: domain, 
		key 	: key, 
		action 	: 'file',
		file 	: parseInt(index)
	}, function(err, body){
		if( err ) return fn( err, null );

		fn(null, jsonResponseHandler(body));
	});
}


module.exports = {
	scan: scanFiles
};
