var express  = require('express');
var users    = require('../lib/users');
var site     = require('../lib/sites');
var utils    = require('../lib/utils');
var validUrl = require('valid-url');

var router  = express.Router();


var responseModel = {
	'error' 	: false,
	'ts'  		: +new Date,
	'msg' 		: null,
	'code' 		: 000,
	'auth' 		: false,
	id: utils.shaHash( (+new Date)+" - "+(Math.random()+"" )
	.split('.')
	.join("")
	.match(/\d{15}$/g))
};

/* GET users listing. */
router.get('/', function(req, res, next) {
	console.log(req.io, router.io);
	res.end('API');
  // res.render('login', { title: 'Express' });
});

/* GET users listing. */
router.get('/:key/site/:id', function(req, res, next) {
	var key = req.params.key;

	if( key !== undefined ){
		key = key.replace('+', '');
		users.byKey( key, function(error, userRecord){

			if( userRecord ){
			
				responseModel.auth = true;
				res.json(responseModel);						
				return;	
			}

			res.json( responseModel );			
			
		} );
		return;
	}	
});

router.get('/:key/crypt', function(req, res, next) {
	var key = req.params.key;

	if( key !== undefined ){
		key = key.replace('+', '');
		users.byKey( key, function(error, userRecord){

			if( userRecord ){
			
				responseModel.auth = true;
				res.json(responseModel);						
				return;	
			}

			responseModel.crypted = utils.hash( (+new Date) +"" );
			res.json( responseModel );			
			
		} );
		return;
	}	
});

router.post('/site/add', function(req, res, next) {
	var siteTemplate = {
		url 	: null,
		active 	: 1,
		date 	: (new Date() ).getTime(),
		hash 	: null,
		user 	: null,
		state 	: ""
	};

	var key = req.body.key;

	delete req.body.key;

	if (!validUrl.isUri(req.body.url) )
		return res.status(200).json({error:true, msg: "Url inválida!"});



	site.add(req.body, function(err, result) {
		console.log(err, '\n->',result);
	});

	res.json({error:false, msg: "Site adicionado."});

});
router.get('/:key', function(req, res, next) {
	var key = req.params.key;
	var resp = {};

	users.byKey( key, function(err, userRecord){



		if( !err ){
			resp.auth = null;
			resp.auth = true;
			return res.status(200).json(Object.assign({
				'error' 	: false,
				'ts'  		: +new Date,
				'msg' 		: null,
				'code' 		: 001,
				id 			: utils.shaHash(Math.random()+""),
				user :  userRecord
			}, resp ));						
	
		}else{
			res.status(401).json(responseModel);
		}
	} );
});


router.post('/site/toggle', function(req, res, next){
	var key = req.body.key;
	var hash = req.body.hash;
	var state = req.body.state;


	var resp = {};
	

	users.byKey( key, function(err, userRecord){
		if( !err ){
			site.byHash( hash, function(error, response){

				if( err ){
					return res.status(500).json({
						error: true, 
						sucess: null, 
						caption: 'Algo deu errado.'
					});
				}

				if( error ){
					res.json(error);
					return;
				}

				site.toggleActive(response, state, function(){
					res.status(200).json({
						error: false, 
						sucess: true, 
						msg: 'Análise enfileirada.'
					});
				});
	

			} );
		}
		else{
			res.status(401).json(responseModel);
		}
	} );
});


router.post('/site/scan', function(req, res, next){
	var key = req.body.key;
	var hash = req.body.hash;
	var resp = {};
	

	users.byKey( key, function(err, userRecord){
		if( !err ){
			site.byHash( hash, function(error, response){

				site.check( response.url, response.hash, function(err, conn){

					if( err ){
						return res.status(500).json({error: true, sucess: null, caption: 'Algo deu errado.'});
					}
					if( error ){
						res.json(error);
						return;
					}

					site.scan(userRecord, response, function(err, scan){
						if( err ){
							res.status(200).json({verbose: err, error: true, sucess: false, msg: 'Algo deu errado.'})

							return;
						}
						res.status(200).json({error: false, sucess: true, msg: 'Análise iniciada.'})
					});
				} );

			} )
		}
		else{
			res.status(401).json(responseModel);
		}
	} );
});

router.get('/:key/sites', function(req, res, next) {
	var key = req.params.key;
	var resp = {};

	users.byKey( key, function(err, userRecord){
		if( !err ){
			resp.auth = true;
			site.get( userRecord.id, function(err, sites) {
				var sites     = sites.result;
				var siteCount = sites.length;
				var record    = null;
				

				if( !err ){
					resp.sites = sites.result;
				}				

				site.getSitesMeta( sites, function(err, meta){
					resp.sites = meta; 		

					// if( resp.sites ){

					// }
					return res.status(200).json(resp);						
				});
			});
		}
		else{
			res.status(401).json(responseModel);
		}
	} );
});

module.exports = router;
