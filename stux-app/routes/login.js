var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
	console.log(req.io, router.io);
  res.render('login', { title: 'Sign In - Stux Net', path: '/login' });
});

module.exports = router;
