var express    = require('express');
var validUrl   = require('valid-url');

var router     = express.Router();
var login      = require('../lib/passport');
var db         = require('../lib/db');
var site       = require('../lib/sites');
var utils      = require('../lib/utils');
var users      = require('../lib/users');
var staticVars = require('../lib/static');
var settings   = require('../lib/settings');
var config 	   = require('../config');


router.get('/me', users.isLoggedIn, function(req, res, next) {

  function renderPage(err, sites){
	
	var sites     = sites.result;
	var siteCount = sites.length;
	var record    = null;

	// site.getSitesMeta( sites, function(err, meta){

		sites.forEach(function(site, index){
			sites[index].dateParsed = utils.formatDate( parseInt( sites[index].date ) )
		});
		req.user.since = utils.formatDate( parseInt( req.user.member_since ) );
		return  res.render('profile', { 
			title 	: 'Perfil - Stux', 
			user 	: req.user, 
			path 	: req.originalUrl, 
			sites  	: sites.reverse()
		});  		 
	
	// });
  } 
  site.get(req.user.id, renderPage)
  
});
/* GET home page. */

router.get('/', users.isLoggedIn, function(req, res, next) {
  function renderPage(errr, sites){
	
	var sites     = sites.result;
	var siteCount = sites.length;
	var record    = null;

	site.getSitesMeta( sites, function(err, meta){

		return  res.render('index', { 
			title 	: 'Sites - Stux', 
			user 	: req.user, 
			path 	: req.originalUrl, 
			sites  	: meta.reverse()
		});  		 
	
	});
  } 
  site.get(req.user.id, renderPage)
  
});

router.get('/auth/google', login.auth, function(req, res){});

router.get('/auth/google/callback', login.cb, function(req, res) {
    res.redirect('/');
});

router.get('/analysis', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}

	users.byId(req.user.id, function(err, user){
		site.userScans(req.user.id, function(err, scans){


			var myScans    = [];
			var hasResult  = null;
			var scanResult = null;

			if( !scans ){
				res.render('analysis', { 
					title 	: 'Análises - Stux',
					path 	: req.originalUrl, 
					user 	: req.user, 
					scans 	: []
				});
				return;
			}

			var result = scans.map(function(scan){

				try{
					scanResult = JSON.parse( scan.result )
				}catch(e){
					scanResult = '{}';
				}

		
				return {
					result 	: scanResult,
					date 	: utils.formatDate( parseInt( scan.date ) ),
					user 	: scan.user,
					id 		: scan.id,
					site 	: scan.site,
					user 	: user,
					percent : scan.percent,
					file 	: scan.file,
					parent_site : scan.parent_meta
				}
			
			
			});
			res.render('analysis', { 
				title 	: 'Análises - Stux',
				path 	: req.originalUrl, 
				user 	: req.user, 
				scans 	: result.reverse() 
			});

		});

	});	
});
router.get('/logout', function(req, res){
	req.logout();
	res.redirect('/login');
});

router.get('/site/add', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	res.render('add-site', { 
		path: req.originalUrl, 
		user: req.user, 
		title: 'Blacklist - Stux',
		hash: utils.hash( ""+ ((+new Date) / .2) ) 
	} );
});
router.get('/settings', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	if( req.user.level == 0 ){
		return res.redirect('/');
	}
	settings.getSettings(function(settings){
		var regex = settings.filter(function(setting){
			return setting.name == 'regex';
		});
		res.render('settings', { 
			path: req.originalUrl, 
			user: req.user, 
			settings: settings,
			regex: regex,
			title: 'Settings - Stux',
			hash: utils.hash( ""+ ((+new Date) / .2) ) 
		} );
	});
});
router.get('/blacklist', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	res.render('blacklist', { 
		path: req.originalUrl, 
		user: req.user, 
		title: 'Adicionar site - Stux',
		hash: utils.hash( ""+ ((+new Date) / .2) ) 
	} );
});
router.post('/settings', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	if( req.user.level == 0 ){
		return res.redirect('/');
	}

	settings.updateSetting('regex', req.body.reg, function(){
		res.json({msg:"Alterações salvas"});
	})
});

router.post('/site-delete', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	if( req.user.level == 0 ){
		return res.redirect('/');
	}

	site.delete(req.body.id, function(){
		res.json({msg:"OK"})
	})

});
router.post('/site-meta', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}
	if( req.user.level == 0 ){
		return res.redirect('/');
	}

	site.updateOutsite(req.body.id, req.body.state, req.body.url, function(){
		res.json({msg:"OK"})
	})

});
router.get('/site/:hash', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}

	site.byHash( req.params.hash, function(error, response){

		if( error ){
			res.json(error);
			return;
		}
		site.userScans(response.user, function(err, scans){

			if( !scans ){
				return res.render('site-single', { path: req.originalUrl, res: response, user: req.user, scans: [] });;
			}
			for( var i = 0; i< scans.length; i++ ){
				scans[i].date = utils.formatDate( parseInt( scans[i].date ) );
				if( i === scans.length-1 ){
					response.date = utils.formatDate( parseInt( response.date ) ).split(' ')[0];
					res.render('site-single', { 
						path: req.originalUrl, 
						res: response, 
						user: req.user,
						scans: scans.reverse(),
						title: 'Site - Stux',
						debug: true
					});
				}
			}
		});
		
	} )
});
router.get('/site/:hash/:id', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}

	var id = req.params.id;

	site.byHash( req.params.hash, function(error, response){

		if( error ){
			res.json(error);
			return;
		}
		site.userScans(response.user, function(err, scans){

			// if( !scans ){
			// 	return res.render('site-single', { path: req.originalUrl, res: response, user: req.user, scans: [] });;
			// }
			var myResult = scans.filter(function(scan){
				return scan.id == id;
			});


		
			myResult[0].date = utils.formatDate( parseInt( myResult[0].date ) );
			utils.parseJSON( myResult[0].result, function(json){

				myResult[0].result = json;
				response.date = utils.formatDate( parseInt( response.date ) ).split(' ')[0];
				res.render('report-single', { 
					path: req.originalUrl, 
					site: response, 
					user: req.user,
					scan: myResult,
					title: 'Relatório - Stux',
					debug: true
				});
			} );
			
		
				
		})
	});
});
router.post('/site/scan', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}


	site.byHash( req.body.hash, function(error, response){

		site.check( response.url, response.hash, function(err, conn){

			if( err ){
				return res.status(500).json({error: true, sucess: null, caption: 'conexão não estabelecida.'});
			}
			if( error ){
				res.json(error);
				return;
			}

			site.scan(req.user, response, function(err, resp){

				if( err ){
					return res.status(500).json(err);
				}
				res.json({msg: 'wait...'})
			});
		} );

	} )
});
router.post('/site/verify', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}

	if (!validUrl.isUri(req.body.url) )
		return res.status(500).json({error:true, success: false, caption: "Url inválida!"});

	site.check( req.body.url, req.body.key, function(error, response){

		if( response ){			
			if( response.state === 'connected' ){
				res.status(200).json({
					state: 'connected', 
					error: null, 
					sucess: true, 
					caption: 'Conexão estabelecida!'
				});
				return;
			}
		}
		if( error ){
			res.status(500).json({
				error: true, 
				sucess: null, 
				caption: 'Conexão não estabelecida.'
			});
		}
	} )
});
router.post('/site/add', function(req, res){
	if( !req.user ){
		return res.redirect('/login');
	}


	if (!validUrl.isUri(req.body.url) )
		return res.status(200).json({error:true, msg: "Url inválida!"});

	site.add( req.body, function(error, response){
		if( error ){
			res.json(error);
			return;
		}

		res.json({sucess: "Website adicionado!"});

	} )
});


router.get('/client/:file', function(req, res, next){


  	var file = req.params.file;
  		
  	// console.log(file, '<<<<<<<<< FILE')

  	utils.clientGenerator( file, res, '', function(e){
  		console.log(e);
  	} );

});


module.exports = router;
