module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: 'public/javascripts',
        concat: {
            js: {
                src: [
                    '<%= path %>/dist/lib/jquery.min.js',
                    '<%= path %>/dist/lib/*.js',
                    '<%= path %>/dist/*.js',
                ],
                dest: '<%= path %>/stux.js',
                options: {
                    separator: ';\n'
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> v<%= pkg.version %>, <%= grunt.template.today("yyyy-mm-dd hh:MM:ss") %> */\n/*! Author: Guilherme Henrique <guihknx@gmail.com> at <apiki.com> Copyright 2017 */\n\n\n'

            },
            js: {
                src: '<%= path %>/stux.js',
                dest: '<%= path %>/stux.min.js'
            }
        },
        watch: {
            js: {
                files: [
                    '<%= path %>/dist/lib/*.js',
                    '<%= path %>/dist/*.js',
                ],
                tasks: ['minify'],
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('minify', ['concat', 'uglify']);
};