var stux = require('./lib');
var server = require('./server');

var options = {
	domain	: process.argv[2] || "",
	key		: process.argv[3] || "",
	verbose	: false,
	timeout : 10 
};

stux.scan(options, function(err, result){

	if( err ) return console.log(err);


	server(result);


});
